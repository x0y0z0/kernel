// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Arbitrary contract generation

use crate::fake_hash::arb_kt1;

use super::Contract;
use proptest::prelude::*;

impl Contract {
    /// Randomly selected originated contract.
    pub fn arb_originated() -> BoxedStrategy<Contract> {
        arb_kt1().prop_map(Contract::Originated).boxed()
    }
}
