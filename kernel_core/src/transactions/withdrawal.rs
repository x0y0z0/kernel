// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Withdrawals

use crate::{
    encoding::{contract::Contract, entrypoint::Entrypoint, string_ticket::StringTicket},
    memory::Memory,
    outbox::{OutboxMessage, OutboxMessageTransaction},
};
use crypto::hash::ContractKt1Hash;
use debug::debug_msg;
use host::{
    rollup_core::{RawRollupCore, MAX_OUTPUT_SIZE},
    runtime::Runtime,
};
use tezos_encoding::enc::BinWriter;

/// Withdrawal to be sent to L1.
#[derive(Debug, PartialEq, Eq)]
pub struct Withdrawal {
    /// Id of the ticket.
    pub ticket: StringTicket,
    /// Destination contract
    pub destination: ContractKt1Hash,
    /// Entrypoint of the destination contract.
    pub entrypoint: Entrypoint,
    /// Amount withdrawn of the ticket.
    pub withdrawn_amount: u64,
}

/// Handle a vector of withdrawals
///
/// The tickets have already been removed from the source account
/// but we need to remove the ticket from durable storage and an
/// withdrawal message goes into the rollup outbox.
pub fn process_withdrawals<Host: RawRollupCore>(
    host: &mut Host,
    withdrawals: Vec<Withdrawal>,
) {
    if withdrawals.is_empty() {
        return;
    }

    let mut transactions = Vec::with_capacity(withdrawals.len());

    for withdrawal in withdrawals.into_iter() {
        // TODO remove `withdrawn_amount` field altogether as it is no longer needed
        let Withdrawal {
            ticket,
            destination,
            entrypoint,
            withdrawn_amount: _ignore,
        } = withdrawal;

        transactions.push(OutboxMessageTransaction {
            parameters: ticket.into(),
            entrypoint,
            destination: Contract::Originated(destination),
        });
    }

    let output = OutboxMessage::AtomicTransactionBatch(transactions.into());

    let mut encoded = Vec::with_capacity(MAX_OUTPUT_SIZE);

    if let Err(err) = output.bin_write(&mut encoded) {
        debug_msg!(Host, "Failed to encode outbox message: {}", err);
    } else {
        // TODO: need to make sure withdrawals will never hit this limit
        // - part of the 'verify' step
        if let Err(err) = host.write_output(encoded.as_slice()) {
            panic!("Failed to write outbox message {:?}", err);
        } else {
            debug_msg!(Host, "Withdrawal executed: {:?}", output);
        }
    }
}

/// Sends given withdrawals to Layer 1, in an [OutboxMessage].
pub fn handle_withdrawals<Host: RawRollupCore>(
    host: &mut Host,
    memory: &mut Memory,
    withdrawals: Vec<Withdrawal>,
) {
    let mut transactions = Vec::with_capacity(withdrawals.len());

    for withdrawal in withdrawals.into_iter() {
        let Withdrawal {
            ticket,
            destination,
            entrypoint,
            withdrawn_amount,
        } = withdrawal;

        let ticket_id = if let Ok(ticket_id) = ticket.identify() {
            ticket_id
        } else {
            debug_msg!(Host, "Failed to get ticket identity");
            return;
        };

        let ticket = memory.withdraw_ticket(ticket_id, withdrawn_amount);

        transactions.push(OutboxMessageTransaction {
            parameters: ticket.into(),
            entrypoint,
            destination: Contract::Originated(destination),
        });
    }

    let output = OutboxMessage::AtomicTransactionBatch(transactions.into());

    let mut encoded = Vec::with_capacity(MAX_OUTPUT_SIZE);
    if let Err(err) = output.bin_write(&mut encoded) {
        debug_msg!(Host, "Failed to encode outbox message: {}", err);
    }

    // TODO: need to make sure withdrawals will never hit this limit
    // - part of the 'verify' step
    if let Err(err) = host.write_output(encoded.as_slice()) {
        debug_msg!(Host, "Failed to write outbox message: {:?}", err);
    } else {
        debug_msg!(Host, "Withdrawal executed: {:?}", output);
    }
}
